import { Component } from '@angular/core';
import { FoodcourtService } from "./Shared/service/foodcourt.service";
import { Foodcourt } from "./Shared/model/foodcourt.model";
import { StallService } from './Shared/service/stall.service';
import { DishesService } from './Shared/service/dishes.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [FoodcourtService,StallService,DishesService]
})
export class AppComponent {
  title = 'app';
}
