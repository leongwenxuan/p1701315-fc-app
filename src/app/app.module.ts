import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';


import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { FoodCourtSummaryComponent } from './foodcourt-summary/foodcourt-summary.component';
import { HeaderComponent } from './header/header.component';
import { AppRoutingModule } from './app-routing.module';
import { StallComponent } from './foodcourt-summary/stall/stall.component';
import { DishesComponent } from './foodcourt-summary/dishes/dishes.component';
import { ReactiveFormsModule } from "@angular/forms";
import { FindPipe } from './Shared/pipe/find.pipe';
import { HoverHighlightDirective } from './Shared/directives/hover-highlight.directive';

import { HttpModule } from "@angular/http";
import { StallService } from './Shared/service/stall.service';
import { FoodcourtService } from './Shared/service/foodcourt.service';
import { RegisterComponent } from './register/register.component';
import { MaincontactComponent } from './contactmain/contactmain.component';
import { ContactAddComponent } from './contactmain/contact-add/contact-add.component';
import { ContactlistComponent } from './contactmain/contactlist/contactlist.component';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    FoodCourtSummaryComponent,
    HeaderComponent,
    StallComponent,
    DishesComponent,
    FindPipe,
    HoverHighlightDirective,
    RegisterComponent,
    MaincontactComponent,
    ContactAddComponent,
    ContactlistComponent
  
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule
  ],
  providers: [StallService,FoodcourtService],
  bootstrap: [AppComponent]
})
export class AppModule { }
