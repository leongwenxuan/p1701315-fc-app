export class Foodcourt {
    constructor(
        public fc_id: number,
        public fc_name: string,
        public location: string,
        public op_hours: string,
        public fc_img01: string,
        public fc_img02: string,
        public description: string
    ) { }
}

export class Stall {
    constructor(
        public st_id: number,
        public fc_id: number,
        public st_name: string,
        public st_img01: string, 
        public description: string
    ) { }
}

export class Dishes {
    constructor (
        public st_id: number,
        public d_id: number,
        public d_name: string,
        public d_img01: string,
        public description: string,
        public price: string,
        public availability: string  ) {}
}

export class Updates {
    constructor (
        public updates_id: number,
        public updates_title: string,
        public description: string
    ) {}
}

export class Register {
    public user_n: string;
    public user_login: string;
    public user_num:string;
    public user_pwd:string;

    constructor ( user_n: string, user_login: string, user_num:string, user_pwd:string) {
         this.user_n = user_n;
         this.user_login = user_login;
         this.user_num = user_num;
         this.user_pwd = user_pwd;
    }
}

export class ContactUs {
    public name: string;
    public email:string;
    public contact:string;
    public information:string;

    constructor ( name: string, email: string, contact:string, information: string) {
         this.name = name;
         this.email = this.email;
         this.contact = this.contact;
         this.information = this.information;
    }
}

