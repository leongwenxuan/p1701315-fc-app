import { Directive, ElementRef, Renderer2, OnInit, HostListener } from '@angular/core';
import { hostElement } from '@angular/core/src/render3/instructions';
import { MockNgModuleResolver } from '@angular/compiler/testing';

@Directive({
  selector: '[appHoverHighlight]'
})
export class HoverHighlightDirective {

  constructor(private elRef:ElementRef, private renderer: Renderer2) { }

  @HostListener ('mouseenter') mouseOver(eventData: Event) {
    this.renderer.setStyle(this.elRef.nativeElement, 'background-color', 'lightblue');
  } 

  @HostListener ('mouseleave') mouseExit(eventData: Event) {
    this.renderer.setStyle(this.elRef.nativeElement, 'background-color', 'transparent');
  }

  // ngOnInit() {
  //   this.renderer.setStyle(this.elRef.nativeElement,'background-color','yellow')
  // }

}
