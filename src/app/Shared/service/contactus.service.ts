import { Injectable, EventEmitter } from '@angular/core';
import { ContactUs } from "../model/foodcourt.model";
import { Http, Response, Headers } from '@angular/http';
import 'rxjs/Rx';

@Injectable()
export class ContactusService {

  contactUsRecieved = new EventEmitter<void>();
  public feedbackCollectionList: ContactUs[] = [];
  constructor(public http: Http) { }

  loadFeedback() {
    // return this.http.get("http://localhost:3000/api/category" + cat_id + "/furniture")
    return this.http.get("/api/contactmain")
      .map((respone: Response) => {
        const data = respone.json();
        this.feedbackCollectionList = data;
        return data;
      },
        (error) => console.log(error)
      );
  }


  addFeedback(ContactUsInfo: ContactUs) {

    const headers = new Headers({ 'Content-Type': 'application/json' });

    this.http.post("/api/contactmain",
      { info: ContactUsInfo },
      { headers: headers })
      .subscribe((response: Response) => {
        const data = response.json();
        this.feedbackCollectionList.push(data);
        this.contactUsRecieved.emit();
      },
        (error) => console.log(error)
      );
  }


  getFeedbacks() {
    return this.feedbackCollectionList.slice();
  }

}