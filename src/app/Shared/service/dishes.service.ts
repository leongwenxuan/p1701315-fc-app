import { Injectable } from '@angular/core';
import { Dishes } from "../model/foodcourt.model";
import { Http, Response, Headers } from "@angular/http";
import "rxjs";

@Injectable()
export class DishesService {

  constructor( private http: Http) {}
  public dishList: Dishes[] = [];

  loadDishes(){
    return this.http.get("/api/dishes")
    .map((response: Response)=>{
      const data = response.json();
      this.dishList = data;
      return data;
    },
    (error)=>console.log(error)
  );
  }

 getDishesByStID(st_id:number): Dishes[] {

    let result: Dishes[] = [];

    for (let dish of this.dishList) {
      if (dish.st_id == st_id) {
        result.push(dish);
      }
    }
    return result.slice();
  }

}
