import { Injectable, EventEmitter } from '@angular/core';
import { Register } from "../model/foodcourt.model";
import { Http, Response, Headers } from "@angular/http";
import 'rxjs/Rx'
@Injectable()
export class RegisterService {
  userAdded = new EventEmitter<void>();
private registerList: Register[]=[];
  constructor(private http: Http) { }

  loadRegister(){
    return this.http.get("/api/register")
      .map((response:Response)=>{
        const data = response.json();
        this.registerList = data;
        return data;
      },
    (error) => console.log(error)
    );
  }
  addRegister(newUserInfo){
    const headers = new Headers({'Content-Type': 'application/json'});

    this.http.post("/api/register",
    {info:newUserInfo},
    {headers: headers})

    .subscribe((response: Response)=>{
      const data = response.json();
      this.registerList.push(data);
      this.userAdded.emit();
    },
  (error)=> console.log(error));
  }
  getRegister(){
    return this.registerList.slice();
  }
}
