import { Injectable } from '@angular/core';
import { Foodcourt } from "../model/foodcourt.model";
import { Http, Response, Headers } from "@angular/http";
import "rxjs";

@Injectable()
export class FoodcourtService {

    private FoodCourtList : Foodcourt[] = [];
    constructor(private http:Http) { }

  
  // public FoodCourtList = [
  //   new Foodcourt(1,"FoodCourt 1","DESIGN SCHOOL BLOCK","Mon-Sat 8AM-8PM","fc1.jpg","Wow Awesome Great Stuff Here"),
  //   new Foodcourt(2,"FoodCourt 2","BESIDE DESIGN","Mon-Sat 8AM-8PM","fc2.jpg","Jezuz The Drinks Are The Best"),
  //   new Foodcourt(3,"FoodCourt 3","NEAR ABE","Mon-Sat 8AM-8PM","fc3.jpg","Mac And Cheese"),
  //   new Foodcourt(4,"FoodCourt 4","BLOCK NEXT TO DESIGN","Mon-Sat 8AM-8PM","fc4.jpg","Awesome Chicken rice"),
  //   new Foodcourt(5,"FoodCourt 5","BESIDE SP GYM","Mon-Sat 8AM-8PM","fc5.jpg","Awesome place for fast food"),
  //   new Foodcourt(6,"FoodCourt 6","DMIT BLOCK","Mon-Sat 8AM-8PM","fc6.jpg","Good Food")
  // ];

 


  // getFoodCourtByFcID(fc_id) {
  //   for(let fc of this.FoodCourtList) {
  //     if(fc.fc_id == fc_id) {
  //       return fc;
  //     }
  //   }
  //   return null;
  // }
  
  loadFoodcourts(){
    return this.http.get("/api/foodcourt")
    .map((respone:Response) =>{
      const data = respone.json();
      this.FoodCourtList = data;
      return data;},
      (error) => console.log(error)
    
    );
  }

  getFoodCourts() {
    return this.FoodCourtList.slice();
  }
}
