import { Injectable } from '@angular/core';
import { Updates } from "../model/foodcourt.model";
import { Http, Response, Headers } from "@angular/http";
import "rxjs";

@Injectable()
export class UpdatesService {

  private UpdateList : Updates[] = []
  constructor(private http: Http) { }

  // public UpdatesList = [
  //   new Updates(1,"New Food","Stall 1 has a new menu"),
  //   new Updates(2,"Maintainence","Foodcourts will be close for maintainence"),
  //   new Updates(3,"Rat","FoodCourt 5 will be close for infestation inspection"),
  //   new Updates(4,"Rat","FoodCourt 5 will be close for infestation inspection")
  // ];
  loadUpdates(){
    return this.http.get("/api/updates")
    .map((respone:Response) =>{
      const data = respone.json();
      this.UpdateList = data;
      return data;},
      (error) => console.log(error)
    
    );
  }
  getUpdates() {
      return this.UpdateList.slice();
  }
}
