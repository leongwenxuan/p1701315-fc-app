import { Injectable } from '@angular/core';
import { Stall,Foodcourt } from "../model/foodcourt.model";
import { Http, Response, Headers } from "@angular/http";
import "rxjs";

@Injectable()
export class StallService {

   StallList : Stall[] = [];
  constructor(private http:Http) { }

  // public StallList = [
  //   new Stall(1,1,"Vegetarian Stall","","Mon-Sat 8AM-8PM","stall1.jpg",""),
  //   new Stall(2,1,"Chicken Rice Stall","","Mon-Sat 8AM-8PM","stall2.jpg",""),
  //   new Stall(3,1,"Indian Stall","","Mon-Sat 8AM-8PM","stall3.jpg",""),
  //   new Stall(4,1,"Japanese Stall","","Mon-Sat 8AM-8PM","stall4.jpg",""),
  //   new Stall(5,1,"Malay Stall","","Mon-Sat 8AM-8PM","stall5.jpg",""),
  //   new Stall(6,1,"Yong Tau Foo Stall","","Mon-Sat 8AM-8PM","stall6.jpg",""),

  //   new Stall(7,2,"Chicken Rice Stall","","Mon-Sat 8AM-8PM","stall2.jpg",""),
  //   new Stall(8,2,"Vegetarian Stall","","Mon-Sat 8AM-8PM","stall3.jpg",""),
  //   new Stall(9,2,"Japanese Stall","","Mon-Sat 8AM-8PM","stall4.jpg",""),
  //   new Stall(10,2,"Drinks Stall","","Mon-Sat 8AM-8PM","stall5.jpg",""),
  //   new Stall(11,2,"Yong Tau Foo Stall","","Mon-Sat 8AM-8PM","stall6.jpg",""),
  //   new Stall(12,2,"Malay Stall","","Mon-Sat 8AM-8PM","stall1.jpg",""),

  //   new Stall(13,3,"Drinks Stall","","Mon-Sat 8AM-8PM","stall1.jpg",""),
  //   new Stall(14,3,"Vegetarian Stall","","Mon-Sat 8AM-8PM","stall4.jpg",""),
  //   new Stall(15,3,"Japanese Stall","","Mon-Sat 8AM-8PM","stall5.jpg",""),
  //   new Stall(16,3,"Yong Tau Foo Stall","","Mon-Sat 8AM-8PM","stall6.jpg",""),
  //   new Stall(17,3,"Chicken Rice Stall","","Mon-Sat 8AM-8PM","stall2.jpg",""),
  //   new Stall(18,3,"Malay Stall","","Mon-Sat 8AM-8PM","stall3.jpg",""),

  //   new Stall(19,4,"Yong Tau Foo Stall","","Mon-Sat 8AM-8PM","stall6.jpg",""),
  //   new Stall(20,4,"Drinks Stall","","Mon-Sat 8AM-8PM","stall4.jpg",""),
  //   new Stall(21,4,"Japanese Stall","","Mon-Sat 8AM-8PM","stall5.jpg",""),
  //   new Stall(22,4,"Western Stall","","Mon-Sat 8AM-8PM","stall1.jpg",""),
  //   new Stall(23,4,"Chicken Rice Stall","","Mon-Sat 8AM-8PM","stall2.jpg",""),
  //   new Stall(24,4,"Fruit Stall","","Mon-Sat 8AM-8PM","stall3.jpg","")

    
  // ];

  // loadStalls(){
  //   return this.http.get("/api/stalls")
  //   .map((response: Response)=>{
  //     const data = response.json();
  //     this.StallList = data;
  //     return data;
  //   },
  //   (error)=>console.log(error)
  // );
  // }
  

  // getStall(fc_id : number){
    
  //   let resultArr: Stall [] = [];
  
    
  //   for (let s of this.StallList) {
  //     if (s.fc_id == fc_id) {
  //       resultArr.push(s);
  //     }
  //   }
  //   return resultArr.slice();
  // }

  getStallByFcID(fc_id): Stall[] {
    let result: Stall[] = [];
    for (let stall of this.StallList) {
      if (stall.fc_id == fc_id) {
        result.push(stall);
      }
    }
    return result.slice();
  }



    getStall(st_id) {
      for (let stall of this.StallList) {
        if (stall.st_id == st_id) {
          return stall;
        }
      }
      return null;
    }
    
  loadStall(){
    return this.http.get("/api/stall")
    .map((respone:Response) =>{
      const data = respone.json();
      this.StallList = data;
      return data;},
      (error) => console.log(error)
    
    );
  }

}
