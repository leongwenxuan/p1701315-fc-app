import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from "@angular/router";
import { FoodCourtSummaryComponent} from "./foodcourt-summary/foodcourt-summary.component";
import { HomeComponent } from "./home/home.component";
import { StallComponent } from './foodcourt-summary/stall/stall.component';
import { DishesComponent } from "./foodcourt-summary/dishes/dishes.component";
import { RegisterService } from './Shared/service/register.service';
import { RegisterComponent } from './register/register.component';
import { MaincontactComponent } from './contactmain/contactmain.component';


const appRoutes: Routes = [
  {path: '', redirectTo: '/home', pathMatch: 'full'},
  {path: 'home', component: HomeComponent},
  {path: 'foodcourt-summary', component: FoodCourtSummaryComponent },
  {path: 'contactmain', component: MaincontactComponent},
  {path: 'stall/:idx', component: StallComponent},
  {path: 'dishes/:idx', component: DishesComponent},
  {path: 'register', component: RegisterComponent}
]


@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes)
  ],
  exports: [
    RouterModule
  ],
  declarations: []
})
export class AppRoutingModule { }
