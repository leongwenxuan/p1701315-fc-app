import { Component, OnInit, Input } from '@angular/core';
import { FoodcourtService } from "../Shared/service/foodcourt.service";
import { Foodcourt } from "../Shared/model/foodcourt.model";
import { Router, ActivatedRoute, Params } from "@angular/router";
import { Stall } from "../Shared/model/foodcourt.model";
import { StallService } from "../Shared/service/stall.service";

@Component({
  selector: 'app-foodcourt-summary',
  templateUrl: './foodcourt-summary.component.html',
  styleUrls: ['./foodcourt-summary.component.css'],
  providers: [FoodcourtService,StallService]
})

export class FoodCourtSummaryComponent implements OnInit {

  foodcourtList : Foodcourt [] = [];     
  stallArr: Stall[];
  searchStr = "";

  constructor(public FoodCourtService : FoodcourtService, public router : Router ,public StallService: StallService ) { }

  ngOnInit ()  {


    this.stallArr = this.StallService.getStallByFcID('idx')
    this.FoodCourtService.loadFoodcourts()
    .subscribe(() => {
      this.foodcourtList = this.FoodCourtService.getFoodCourts();
    });
    // this.FoodcourtArr = this.FoodCourtService.getFoodCourts();
    // this.stallArr = this.StallService.getStallByFcID('idx');


  }

    onViewStallsDetails(index: number) {
      this.router.navigate(['/stall', index]);
    }

}

