import { Component, OnInit } from '@angular/core';
import { Dishes } from "../../Shared/model/foodcourt.model";
import { DishesService } from "../../Shared/service/dishes.service";


import { Router, ActivatedRoute, Params } from "@angular/router";
import { StallService } from '../../Shared/service/stall.service';


@Component({
  selector: 'app-dishes',
  templateUrl: './dishes.component.html',
  styleUrls: ['./dishes.component.css']
})
export class DishesComponent implements OnInit {

  selectedDishes: Dishes[];


  constructor(public router: Router, public route: ActivatedRoute,public stallService: StallService,public DishesService: DishesService) { }
  
  searchStr = "";


  ngOnInit() {
    // const index: number = this.route.snapshot.params['idx'];
    const st_id: number = this.route.snapshot.params['idx'];
    this.DishesService.loadDishes()
    .subscribe(() => {
      this.selectedDishes = this.DishesService.getDishesByStID(st_id);
    })

  }

}
