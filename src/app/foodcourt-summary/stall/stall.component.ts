import { Component, OnInit } from '@angular/core';
import { StallService } from "../../Shared/service/stall.service";
import { Stall, Dishes } from "../../Shared/model/foodcourt.model";
import { Router, ActivatedRoute, Params } from "@angular/router";
// import { constants } from 'os';


@Component({
  selector: 'app-stall',  
  templateUrl: './stall.component.html',
  styleUrls: ['./stall.component.css']
})
export class StallComponent implements OnInit {

  // StallList: Stall[] = []
  selectedFC: Stall [];
  searchStr ="";

  constructor(public router: Router,
   public route: ActivatedRoute,
   public stallService: StallService) { }





  ngOnInit() {

    // const index: number = this.route.snapshot.params['idx'];
    // this.stallService.loadStalls()
    // .subscribe(()=>{
    //   this.selectedFC = this.stallService.getStallByFcID(index);
    //   if (this.selectedFC == null) {
    //     this.router.navigate(['/not-found'])
    //   }   
    // })
    const index : number = this.route.snapshot.params ['idx'];
    this.stallService.loadStall()
    .subscribe(() => {
      this.selectedFC = this.stallService.getStallByFcID(index);
      if(this.selectedFC == null) {
        this.router.navigate(['/not-found'])
      }
      // this.StallList = this.stallService.getAllStall();
      
    });
  }
  onViewDishesDetails(index: number) {
    this.router.navigate(['/dishes', index]);
  }

  // viewDetails(st_id) {
  //   this.router.navigate([st_id], {relativeTo: this.route});
  // }

}
