import { Component, OnInit} from '@angular/core';
import { Register } from "../Shared/model/foodcourt.model";
import {RegisterService } from "../Shared/service/register.service";
import { FormGroup, FormControl, Validators } from '@angular/forms';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
  providers:[RegisterService]
})
export class RegisterComponent implements OnInit {
  inputInfo: Register = new Register("","","","");
  userList: Register [] =[];
  emailBlackList = ['test@test.com', 'temp@temp.com'];
  signupForm: FormGroup;
  submitted:boolean=false;
  constructor(private RegisterService : RegisterService) { }

  ngOnInit() {
    this.RegisterService.loadRegister()
    .subscribe((result)=>{
      this.userList = this.RegisterService.getRegister()
    });
    
    this.RegisterService.userAdded
    .subscribe(()=>{
      this.userList= this.RegisterService.getRegister();
    });

    this.signupForm = new FormGroup({
      'NameAdd': new FormControl(null, [Validators.required, this.blankSpaces]),
      'EmailAdd': new FormControl(null, [Validators.required, Validators.email, this.inEmailBlackList.bind(this)]),
      'ContactAdd': new FormControl(null, [Validators.required, this.blankSpaces]),
      'PasswordAdd': new FormControl(null, [Validators.required, this.blankSpaces, this.NumCheckPw, this.PassChk])
    });
  }
  blankSpaces(control: FormControl): { [s: string]: boolean } {
    if (control.value != null && control.value.trim().length === 0) {
      return { 'blankSpaces': true };
    }
    return null;
  }

  inEmailBlackList(control: FormControl): { [s: string]: boolean } {
    if (this.emailBlackList.indexOf(control.value) !== -1) {
      return { 'emailBlackListed': true };
    }
    return null;
  }
  NumCheckPw(control: FormControl): { [s: string]: boolean } {
    var Num = /[0-9]/;
    var TestNum = new RegExp(Num);
    var Pass = control.value;
    if (control.value != null && TestNum.test(Pass)==false) {
      return { 'NumCheckPw': true };
    }
    return null;
  }
  PassChk(control: FormControl): { [s: string]: boolean } {
    var lowerCase = /[a-z]/;
    var upperCase = /[A-Z]/;
    var specialCase = /[@!#$%^&*]/;
    var lowerCaseTest = new RegExp(lowerCase);
    var upperCaseTest = new RegExp(upperCase);
    var specialCaseTest = new RegExp(specialCase);
    var password = control.value;
    if (control.value != null && (upperCaseTest.test(password)&& lowerCaseTest.test(password)&& specialCaseTest.test(password)) == false) {
      return { 'PassChk': true };
      }
    return null;
  }
onUserAdded(newUserInfo){
  console.log(newUserInfo);
  this.userList.push(newUserInfo);
  console.log(this.userList);
}
onAddUser(inputName,inputEmail,inputContact, inputPw: HTMLInputElement){
  console.log("Add User");
  console.log(this.inputInfo);
  console.log(inputName.value);
  console.log(inputEmail.value);
  console.log(inputContact.value);
  console.log(inputPw.value);
  this.submitted = true;
  this.RegisterService.addRegister(new Register(
    inputName.value,
    inputEmail.value,
    inputContact.value,
    inputPw.value
  ))
}
aftSubmit(){
  // this.contactForm.reset();
  // this.submitted = false;
  this.submitted=false;
  this.signupForm.reset({
    "NameAdded": "",
    "EmailAdded":"",
    "ContactAdded":"",
    "PasswordAdded":""
      });
}
}
