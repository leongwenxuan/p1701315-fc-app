import { Component, OnInit} from '@angular/core';
import { ContactUs } from "../../Shared/model/foodcourt.model";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { ContactusService } from "../../Shared/service/contactus.service";

@Component({
  selector: 'app-contact-add',
  templateUrl: './contact-add.component.html',
  styleUrls: ['./contact-add.component.css']
})
export class ContactAddComponent implements OnInit {

  inputInfo: ContactUs = new ContactUs("", "", "", "");

  feedbackform: FormGroup
  submitted = false;
  emailBlackList = ['test@test.com', 'temp@temp.com', 'abc@abc.com', 'a@a'];
  nameBlackList = ['Lenny', 'abc'];

  
  constructor(public ContactusService: ContactusService) { }

  ngOnInit() {
    this.feedbackform = new FormGroup({
      'contact': new FormControl(null, [Validators.required]),
      'message': new FormControl(null, [Validators.required, this.blankSpaces]),
      'name': new FormControl(null, [Validators.required, this.blankSpaces, this.inNameBlackList.bind(this)]),
      'email': new FormControl(null, [Validators.required, Validators.email, this.blankSpaces, this.inEmailBlackList.bind(this)])
    });

    console.log(this.feedbackform);

  }

  onSubmit(inputName, inputEmail,inputContact,inputInfo: HTMLInputElement) {
    // console.log(this.feedbackform.value);
    this.submitted = true;
    // console.log(this.inputName.n


    this.ContactusService.addFeedback(new ContactUs(
      inputName.value,
      inputEmail.value,
      inputContact.value,
      inputInfo.value
    ));

  }

  blankSpaces(control: FormControl): { [s: string]: boolean } {
    if (control.value != null && control.value.trim().length === 0) {
      return { 'blankSpaces': true };
    }
    return null;
  }

  inEmailBlackList(control: FormControl): { [s: string]: boolean } {
    if (this.emailBlackList.indexOf(control.value) !== -1) {
      return { 'emailBlackListed': true };
    }
    return null;
  }

  inNameBlackList(control: FormControl): { [s: string]: boolean } {
    if (this.nameBlackList.indexOf(control.value) !== -1) {
      return { 'nameBlackListed': true };
    }
    return null;
  }



}
