import { Component, OnInit, Input } from '@angular/core';
import { ContactUs } from '../../Shared/model/foodcourt.model';
import { Router } from "@angular/router";
import { ContactusService } from '../../Shared/service/contactus.service';


@Component({
  selector: 'app-contactlist',
  templateUrl: './contactlist.component.html',
  styleUrls: ['./contactlist.component.css'],
})
export class ContactlistComponent implements OnInit {

  @Input() ContactCollected: ContactUs[];
  searchStr : String ="";
  searchFoodStr : String ="";
  submitted: boolean=false;
  constructor(public router: Router) { }

  ngOnInit() {
    // this.feedbackCollected = this.feedbackservice.getFeedbacks();
  }
  // onViewDetails(index: number) {
  //   this.router.navigate(['/contactusDetail', index]);
  // }
}
