import { Component, OnInit } from '@angular/core';
import { ContactUs } from "../Shared/model/foodcourt.model";
import { ContactusService } from "../../app/Shared/service/contactus.service";

@Component({
  selector: 'app-contactmain',
  templateUrl: './contactmain.component.html',
  styleUrls: ['./contactmain.component.css'],
  providers: [ContactusService]
})
export class MaincontactComponent implements OnInit {
  ContactUsList: ContactUs[] = [];

  constructor(public ContactusService: ContactusService) { }

  ngOnInit() {

    this.ContactusService.loadFeedback()
      .subscribe((result) => {
        this.ContactUsList = this.ContactusService.getFeedbacks();
      });

    this.ContactusService.contactUsRecieved
      .subscribe((result) => {
        this.ContactUsList = this.ContactusService.getFeedbacks();
      });

  }

  // (contactUsRecieved) = "onContactUsRecieved($event)"
  onFeedbackAdded(newFeedbackInfo) {
    console.log(newFeedbackInfo);
    this.ContactUsList.push(newFeedbackInfo);
    console.log(this.ContactUsList);
  }
}
