import { Component, OnInit } from '@angular/core';
import { DishesService } from "../Shared/service/dishes.service";
import { UpdatesService } from '../Shared/service/updates.service';
import { Updates, Foodcourt } from "../Shared/model/foodcourt.model";
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [UpdatesService]
})
export class HomeComponent implements OnInit {

  UpdateList : Updates[] = [];

  constructor( private UpdateService: UpdatesService, public route: ActivatedRoute, public router: Router) { }

  ngOnInit() {

    this.UpdateService.loadUpdates()
    .subscribe(() => {
      // this.UpdateList = this.UpdateService.loadUpdates();
      this.UpdateList = this.UpdateService.getUpdates();
    });
  }

}
